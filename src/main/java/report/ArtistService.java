package report;

import java.util.List;

public class ArtistService {
    public List<ArtistReport> getTopTenArtistByTotalPrice(){
        ArtistDao artistDao = new ArtistDao();
        return  artistDao.getArtistByTotalPrice(10);
    }
    
    public List<ArtistReport> getTopTenArtistByTotalPrice(String begin,String end){
        ArtistDao artistDao = new ArtistDao();
        return  artistDao.getArtistByTotalPrice(begin,end,10);
    }
}
